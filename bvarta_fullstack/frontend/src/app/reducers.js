import {
    GET_CROWD_POINT_LIST_START,
    GET_CROWD_POINT_LIST_SUCCESS
} from './actionCreators';

const crowdPointList = (state = {}, action) => {
    if (GET_CROWD_POINT_LIST_START === action.type) {
        const { value } = state;
        return {
            value,
            isLoaded: !!value,
            isUpdating: true
        };
    }
    if (GET_CROWD_POINT_LIST_SUCCESS === action.type) {
        const { value } = action.payload;
        return {
            value,
            isLoaded: true,
            isUpdating: false
        };
    }
    return state;
};

export default {
    crowdPointList
};