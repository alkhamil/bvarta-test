import React, { Component, useRef } from 'react';
import L from 'leaflet';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';
import { connect } from "react-redux";

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            map: null
        };
    }
    
    render() {
        const { name, latitude, longitude } = this.props.currentRegionValue;
        const { map } = this.state;

        if (map) {
            map.target.flyTo([latitude, longitude], 13, {
                duration: 2
            });
        }
        return (
            <MapContainer whenReady={(map) => this.setState({ map })} center={[latitude, longitude]} zoom={13} scrollWheelZoom={false}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    className='leaflet-tiles'
                />
                <Marker position={[latitude, longitude]}>
                    <Popup>
                        {name}
                    </Popup>
                </Marker>
            </MapContainer>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentRegionValue: state.currentRegion
    }
}

export default connect(mapStateToProps)(Map)