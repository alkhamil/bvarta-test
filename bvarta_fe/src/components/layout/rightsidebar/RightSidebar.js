import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import ActionType from '../../../app/actionType';
import './RightSidebar.scss'

class RightSidebar extends Component {
    render() {
        const { name } = this.props.currentRegionValue;

        return (
            <Fragment>
                <div onClick={this.props.toggleRightSidebar} className='toggle-right-sidebar'>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div className={this.props.toggleRightSidebarValue ? 'right-sidebar hide' : 'right-sidebar'}>
                    {this.props.listRegionValue.map((item, key) => {
                        return <li className={name == item.name ? 'active' : ''} onClick={() => this.props.changeRegion(item.name)} key={key}>{item.name}</li>
                    })}
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        toggleRightSidebarValue: state.toggleRightSidebar,
        listRegionValue: state.listRegion,
        currentRegionValue: state.currentRegion
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleRightSidebar: () => {
            dispatch({ type: ActionType.TOGGLE_RIGHT_SIDEBAR });
        },
        changeRegion: (regionName) => {
            dispatch({ type: ActionType.CHANGE_REGION, regionName: regionName })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RightSidebar);