import React from 'react'
import { useTranslation } from 'react-i18next';
import { connect } from "react-redux";
import ActionType from '../../../app/actionType';
import "./Header.scss";


const Header = (props) => {
    const { t, i18n } = useTranslation();

    const changeLanguage = (lng) => {
        i18n.changeLanguage(lng);
    }

    return (
        <div className='header'>
            <div className='logo'>
                <span className='brand-name'>
                    BVARTA TEST
                </span>
                <span className='toggle-left-sidebar' onClick={props.toggleLeftSidebar}>
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </div>
            <div className='app-name'>
                {t('app_name')}
            </div>
            <div className='toolbar'>
                <div onClick={props.toggleTheme} className='theme'>
                    <img src={props.toggleThemeValue ? 'dark-mode.png' : 'light-mode.png'} alt="ok" />
                </div>

                <div className='language'>
                    <select onChange={e => changeLanguage(e.target.value)}>
                        <option value="id">Bahasa</option>
                        <option value="en">English</option>
                    </select>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        toggleLeftSidebarValue: state.toggleLeftSidebar,
        toggleThemeValue: state.toggleTheme
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleLeftSidebar: () => {
            dispatch({type: ActionType.TOGGLE_LEFT_SIDEBAR});
        },

        toggleTheme: () => {
            dispatch({type: ActionType.CHANGE_THEME});
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
