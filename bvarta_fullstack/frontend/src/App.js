import React, { Component, Fragment } from 'react';
import Map from './componets/map/Map';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Map />
      </Fragment>
    );
  }
}

export default App;