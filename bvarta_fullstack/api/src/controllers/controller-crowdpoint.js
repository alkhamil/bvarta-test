const config = require('../configs/database');
const mysql = require('mysql');
const pool = mysql.createPool(config);

pool.on('error', (err) => {
    console.error(err);
});

module.exports = {
    getCrowdPoint(req, res) {
        pool.getConnection(function (err, connection) {
            if (err) throw err;
            connection.query(
                `
                SELECT
                    a.longitude,
                    a.latitude,
                    a.brand,
                    CONCAT (
                        (SELECT MIN(time) FROM bvarta_api.tr_crowdpoint WHERE DATE_FORMAT( time, "%h:%i" ) BETWEEN '07:00' AND '08:00'),
                        ' to ',
                        (SELECT MAX(time) FROM bvarta_api.tr_crowdpoint WHERE DATE_FORMAT( time, "%h:%i" ) BETWEEN '07:00' AND '08:00')
                    ) AS ranges,
                    SUM(a.user_count) AS user_per_brand,
                    (SELECT SUM(user_count) FROM bvarta_api.tr_crowdpoint  WHERE longitude = a.longitude AND latitude = a.latitude) AS user_count
                FROM
                    bvarta_api.tr_crowdpoint AS a
                WHERE
                    DATE_FORMAT( a.time, "%h:%i" ) BETWEEN '07:00' AND '08:00'
                GROUP BY
                    a.longitude,
                    a.latitude,
                    a.brand
                `
                , function (error, results) {
                    if (error) throw error;
                    res.send(results);
                });
            connection.release();
        })
    }
}