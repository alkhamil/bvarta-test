import React, { Component } from 'react';
import { connect } from "react-redux";
import ActionType from '../../../app/actionType';
import "./LeftSidebar.scss";

class LeftSidebar extends Component {
    render() {
        const { name } = this.props.currentRegionValue;

        return (
            <div className={this.props.toggleLeftSidebarValue ? 'left-sidebar' : 'left-sidebar toggled'}>
                {this.props.listRegionValue.map((item, key) => {
                    return <li className={name == item.name ? 'active' : ''} onClick={() => this.props.changeRegion(item.name)} key={key}>{item.name}</li>
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        toggleLeftSidebarValue: state.toggleLeftSidebar,
        listRegionValue: state.listRegion,
        currentRegionValue: state.currentRegion
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeRegion: (regionName) => {
            dispatch({ type: ActionType.CHANGE_REGION, regionName: regionName })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LeftSidebar);