import { createStore } from "redux";
import listRegion from "../data/region";
import ActionType from "./actionType";

const initialState = {
    toggleTheme: false,
    toggleLeftSidebar: true,
    toggleRightSidebar: true,
    listRegion: listRegion,
    currentRegion: {
        name: 'Jakarta, Indonesia',
        latitude: -6.200000,
        longitude: 106.816666
    },
};

const rootReducer = (state = initialState, action) => {

    switch (action.type) {
        case ActionType.CHANGE_THEME:
            const toggleTheme = state.toggleTheme = !state.toggleTheme;
            return {
                ...state,
                toggleTheme: toggleTheme
            }
        case ActionType.TOGGLE_LEFT_SIDEBAR:
            const toggleLeftSidebar = state.toggleLeftSidebar = !state.toggleLeftSidebar;
            return {
                ...state,
                toggleLeftSidebar: toggleLeftSidebar
            }
        case ActionType.TOGGLE_RIGHT_SIDEBAR:
            const toggleRightSidebar = state.toggleRightSidebar = !state.toggleRightSidebar;
            return {
                ...state,
                toggleRightSidebar: toggleRightSidebar
            }
        case ActionType.CHANGE_REGION:
            const currentRegion = state.listRegion.find(item => item.name === action.regionName);
            return {
                ...state,
                currentRegion: currentRegion
            }
        default:
            break;
    }
    
    return state;
}

const store = createStore(rootReducer);

export default store