import fetch from 'cross-fetch';

export const GET_CROWD_POINT_LIST_START = 'GET_CROWD_POINT_LIST_START';
export const GET_CROWD_POINT_LIST_SUCCESS = 'GET_CROWD_POINT_LIST_SUCCESS';

const getCrowdPointListStart = (value) => ({
    type: GET_CROWD_POINT_LIST_START,
    payload: { value }
});

const getCrowdPointListSuccess = (value) => ({
    type: GET_CROWD_POINT_LIST_SUCCESS,
    payload: { value }
});

export const getCrowdPointList = () => dispatch => {
    dispatch(getCrowdPointListStart());
    return fetch(`http://localhost:8080/crowd-point`)
        .then(response => response.json())
        .then(json => dispatch(getCrowdPointListSuccess(json)));
};