/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : bvarta_api

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 17/07/2022 16:44:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tr_crowdpoint
-- ----------------------------
DROP TABLE IF EXISTS `tr_crowdpoint`;
CREATE TABLE `tr_crowdpoint`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `longitude` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `latitude` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `brand` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `time` datetime NULL DEFAULT NULL,
  `user_count` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tr_crowdpoint
-- ----------------------------
INSERT INTO `tr_crowdpoint` VALUES (1, '106.77709118108051', '-6.142837378870265', '	Infinix', '2021-10-20 08:00:00', 1);
INSERT INTO `tr_crowdpoint` VALUES (2, '106.77709118108051', '-6.142837378870265', '	Samsung', '2021-10-20 07:00:00', 1);
INSERT INTO `tr_crowdpoint` VALUES (3, '106.77809118108054', '-6.142837378870266', '	Xiaomi	', '2021-10-20 09:00:00', 1);
INSERT INTO `tr_crowdpoint` VALUES (4, '106.7790911810806', '-6.142837378870266', '	Xiaomi	', '2021-10-20 07:00:00', 1);
INSERT INTO `tr_crowdpoint` VALUES (5, '106.7790911810806', '-6.142837378870266', '	Oppo	', '2021-10-20 07:00:00', 5);
INSERT INTO `tr_crowdpoint` VALUES (6, '106.7790911810806', '-6.142837378870266', '	Xiaomi	', '2021-10-20 08:00:00', 2);
INSERT INTO `tr_crowdpoint` VALUES (7, '106.78109118108071', '-6.142837378870266', '	Itel	', '2021-10-20 08:00:00', 8);
INSERT INTO `tr_crowdpoint` VALUES (8, '106.78209118108074', '-6.142837378870266', '	Huawei	', '2021-10-20 08:00:00', 6);
INSERT INTO `tr_crowdpoint` VALUES (9, '106.78209118108074', '-6.142837378870266', '	Samsung', '2021-10-20 08:00:00', 1);
INSERT INTO `tr_crowdpoint` VALUES (10, '106.78209118108074', '-6.142837378870266', '	Samsung', '2021-10-20 07:00:00', 2);

SET FOREIGN_KEY_CHECKS = 1;
