import React, { Component } from 'react'
import Header from './components/layout/header/Header'
import LeftSidebar from './components/layout/leftsidebar/LeftSidebar'
import Map from './components/map/Map'
import { connect } from "react-redux"
import "./App.css";
import RightSidebar from './components/layout/rightsidebar/RightSidebar'

class App extends Component {
  render() {
    return (
      <div className={this.props.toggleThemeValue ? 'app-layout dark' : 'app-layout'}>
        <Header />
        <div className={ this.props.toggleLeftSidebarValue ? 'app-content' : 'app-content toggled'}>
          <LeftSidebar />
          <Map />
          <RightSidebar />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    toggleLeftSidebarValue: state.toggleLeftSidebar,
    toggleThemeValue: state.toggleTheme
  }
}
export default connect(mapStateToProps)(App)
