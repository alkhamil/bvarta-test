import React, { Component } from 'react';
import L from 'leaflet';
import { Circle, CircleMarker, MapContainer, Marker, Popup, TileLayer, Tooltip } from 'react-leaflet';
import { connect } from "react-redux";
import './Map.scss';
import * as actionCreators from '../../app/actionCreators';
import PropTypes from 'prop-types';
import { selectCrowdPointList } from '../../app/selectors';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

class Map extends Component {

    static propTypes = {
        getCrowdPointList: PropTypes.func.isRequired,
        crowdPointList: PropTypes.shape({
            value: PropTypes.arrayOf(PropTypes.object),
        })
    };

    componentDidMount() {
        this.props.getCrowdPointList();
    }

    render() {
        const { crowdPointList } = this.props;
        const position = {
            lat: -6.142837378870266,
            lng: 106.7790911810806
        }

        if (!crowdPointList || !crowdPointList.isLoaded) {
            return 'Loading Map list...';
        }

        var countTotal = 0;
        var ranges = '';
        crowdPointList.value.map((item, i) => {
            if (i == 0) {
                ranges = item.ranges;
            }
            countTotal += item.user_count;
        })

        return (
            <MapContainer center={[position.lat, position.lng]} zoom={16} scrollWheelZoom={true}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <CircleMarker
                    center={{ lat: position.lat, lng: position.lng }}
                    fillColor="green"
                    color='green'

                    radius={20}>
                    <Tooltip direction="center" offset={[0, 0]} opacity={1} permanent>{countTotal}</Tooltip>
                    <Popup className='card-popup'>
                        <div className='card-header'>Information</div>
                        <div className='card-body'>
                            {ranges} <strong>({countTotal})</strong>

                            <table>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        crowdPointList.value.map((item, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td>{item.brand}</td>
                                                    <td>{item.user_count}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </Popup>
                </CircleMarker>
            </MapContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    crowdPointList: selectCrowdPointList(state)
});

const mapDispatchToProps = {
    getCrowdPointList: actionCreators.getCrowdPointList
};

export default connect(mapStateToProps, mapDispatchToProps)(Map)