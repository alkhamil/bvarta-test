const router = require('express').Router();
const { crowdpoint } = require('../controllers');

router.get('/crowd-point', crowdpoint.getCrowdPoint);

module.exports = router;